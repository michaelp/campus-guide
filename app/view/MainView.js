Ext.define('CampusGuide.view.MainView',
{
    extend: 'Ext.NavigationView',
    xtype: 'main',
    //title: 'Campus Guide',
    config:
    {
    	layout: 'card',
    	
        items:
        [
            {
                xtype: 'maintab',
            }
        ]
    }
});
