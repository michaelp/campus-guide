Ext.define( 'CampusGuide.view.BuildingsView',
{
	extend: 'Ext.Container',
	xtype: 'buildings',
	
	config:
	{
		layout: 'vbox',
		items:
		[
		{
			xtype: 'label',
			html: 'Where do you want to go?',
			flex: 1
		},
		{
			xtype: 'container',
			layout: 'vbox',
			defaults:
			{
				xtype: 'buildingrow'
			},
			items:
			[
			{
				labels: [ 'Class', 'Gym', 'Library' ]
			},
			{
				labels: [ 'Residence', 'Washroom', 'Office' ]
			},
			{
				labels: [ '...', 'Campus Facility', 'Other' ]
			}
			],
			flex: 5
		}
		]
	}
} );
