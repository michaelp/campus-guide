/*
 * View used to display a list of buildings in a certain category
 * Class, food, gym, etc
 */
Ext.define( 'CampusGuide.view.BuildingListView',
{
	extend: 'Ext.dataview.List',
	xtype: 'buildinglist',
	
	config:
	{
		itemTpl: '{name}'
	}
} );
