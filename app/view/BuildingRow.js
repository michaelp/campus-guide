Ext.define( 'CampusGuide.view.BuildingRow',
{
	extend: 'Ext.Container',
	xtype: 'buildingrow',
	
	config:
	{
		layout: 'hbox',
		flex: 1,
		labels: [],
		items:
		[
		{
			xtype: 'buildingbutton'
		},
		{
			xtype: 'buildingbutton'
		},
		{
			xtype: 'buildingbutton'
		}
		]
	},
	
	initialize: function( row, eOpts )
	{
		this.callParent();
		for( var i = 0; i < this.getLabels().length; i++ )
		{
			this.getItems().items[i].setHtml( this.getLabels()[i] );
		}
	}
} );
