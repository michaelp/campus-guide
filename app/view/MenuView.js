Ext.define( 'CampusGuide.view.MenuView',
{
	extend: 'Ext.Container',
	xtype: 'menu',
	
	config:
	{
		layout: 'vbox',
		items:
		[
		{
			flex: 1,
			xtype: 'label',
			html: 'Current University:',
			style: 'border: solid 1px black;',
			//Give it an ID so that we can change the HTML when necessary
			id: 'currentUniLabel'
		},
		{
			flex: 5,
			xtype: 'list',
			itemTpl: '{name}',
			title: CampusGuide.app.currentUni,
			ui: 'round',
			data:
			[
				{ name: 'Classes' },
				{ name: 'Food' },
				{ name: 'Leisure' },
				{ name: 'Options' }
			]
		}
		]
	}
} );
