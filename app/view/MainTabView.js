Ext.define( 'CampusGuide.view.MainTabView',
{
	extend: 'Ext.tab.Panel',
	xtype: 'maintab',
	
	config:
	{
		tabBarPosition: 'bottom',
		items:
		[
		{
			title: 'Home',
			iconCls: 'home',	
			xtype: 'menu'
		},
		{
			title: 'Buildings',
			iconCls: 'screens',
			xtype: 'buildings'
		},
		/*{
			title: 'Food',
			iconCls: 'user',
			xtype: 'food'
		},*/
		{
			title: 'Search',
			iconCls: 'search',
			xtype: 'search'
		}
		]
	}
} );
