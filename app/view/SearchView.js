Ext.define( 'CampusGuide.view.SearchView',
{
	extend: 'Ext.Container',
	xtype: 'search',
	
	config:
	{
		layout: 'vbox',
		defaults:
		{
			flex: 1
		},
		items:
		[
		{
			xtype: 'label',
			html: 'Label'
		},
		{
			xtype: 'searchfield',
			label: 'Search',
			name: ''
		},
		{
			xtype: 'container',
			html: 'results'
		}
		]
	}
} );
