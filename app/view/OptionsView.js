Ext.define( 'CampusGuide.view.OptionsView',
{
	extend: 'Ext.form.Panel',
	config:
	{
		items:
		[
		{
			xtype: 'selectfield',
			name: 'uniName',
			label: 'Current University',
			store: 'Universities',
			valueField: 'name',
			displayField: 'name'
		}
		]
	}
} );
