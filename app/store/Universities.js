Ext.define( 'CampusGuide.store.Universities',
{
	extend: 'Ext.data.Store',
	config:
	{
		model: 'CampusGuide.model.University',
		autoLoad: true
	}
} );
