Ext.define( 'CampusGuide.controller.CampusMapController',
{
	extend: 'Ext.app.Controller',
	
	config:
	{
		refs:
		{
			campusMap: 'map'
		},
		control:
		{
			campusMap:
			{
			   maprender: 'onMapRender'
			}
		}
	},
	
	onMapRender: function(expmap, map, objs)
	{
	   var geocoder = new google.maps.Geocoder();
		geocoder.geocode( { address: CampusGuide.app.currentUni.get('name') }, function(results, status)
		{
			if( status == google.maps.GeocoderStatus.OK )
			{
			   var location = results[0].geometry.location;
			   var maker = new google.maps.Marker( { map: expmap.getMap(), position: location } );
				expmap.getMap().setCenter( location );
			}
		});
	}
} );
