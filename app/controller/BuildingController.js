/*
 * Handle events that deal with the building tab
 */
Ext.define( 'CampusGuide.controller.BuildingController',
{
	extend: 'Ext.app.Controller',
	
	config:
	{
		refs:
		{
			buildingButtons: 'buildingbutton',
			buildingList: 'buildinglist',
			navView: 'navigationview',
			campusMap: 'map'
		},
		control:
		{
			buildingButtons:
			{
				tap: 'onBuildingButtonClick'
			},
			buildingList:
			{
				itemtap: 'directionsToBuilding'
			}
		}
	},
	
	onBuildingButtonClick: function(button, evt, eOpts)
	{
		var buttonType = button.getHtml();
		if( buttonType == "Class" )
		{
			this.getNavView().push(
			{
				xtype: 'buildinglist',
				store: CampusGuide.app.currentUni.buildings()
			} );
		}
	},
	
	directionsToBuilding: function( list, index, elem, record, evt, eOpts )
	{
		this.getNavView().push( { xtype: 'map' } );
		this.getBuilding( record );
	},
	
	getBuilding: function( record )
	{
		//see http://training.figleaf.com/tutorials/senchacomplete/chapter2/lesson9/7.cfm
		var controller = this;
		var map = this.getCampusMap();
		if( map.getMap() == null )
		{
			Ext.defer( function() { this.getBuilding(record) }, 250, this );
		}
		
		var searchFor = CampusGuide.app.currentUni.get('name') + " " + record.get('name');
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode( { address: searchFor }, function(results, status)
		{
			if( status == google.maps.GeocoderStatus.OK )
			{
			   var location = results[0].geometry.location;
			   var marker = new google.maps.Marker( { map: map.getMap(), position: location } );
			   controller.getDirections(map, location);
			}
		});
		
		
	},
	
	getDirections: function(map, dest)
	{
		//Get directions
		var directionsService = new google.maps.DirectionsService();
		getCurrentLocation(function(pos)
		{
			var request =
			{
				origin: pos,
				destination: dest,
				travelMode: google.maps.TravelMode.WALKING
			};
			directionsService.route( request, function( result, status )
			{
				if( status == google.maps.DirectionsStatus.OK )
				{
					var renderer = new google.maps.DirectionsRenderer();
					renderer.setMap( map.getMap() );
					renderer.setDirections( result );
				}
			} );
		});
	}
} );
