Ext.define( 'CampusGuide.controller.MainController',
{
	extend: 'Ext.app.Controller',
	
	config:
	{
		refs:
		{
			menu: 'menu list',
			navView: 'navigationview',
			uniPick: 'picker',
		},
		control:
		{
			menu:
			{
				itemtap: 'onMenuTap'
			},
			uniPick:
			{
				change: 'onUniPick',
				cancel: 'onNoUniPick'
			}
		}
	},
	
	onMenuTap: function( dView, index, target, record, e, eOpts )
	{
		switch(index)
		{
			case 0:
				var map = Ext.create( 'CampusGuide.view.CampusMapView' );
				this.getNavView().push( map );
				break;
			case 1:
				break;
			case 2:
				break;
			case 3:
				var options = Ext.create( 'CampusGuide.view.OptionsView' );
				this.getNavView().push( options );
				break;
			default:
				break;
		}
	},
	
	onUniPick: function( picker, obj, eOpts )
	{
		CampusGuide.app.currentUni = obj.unis;
		Lawnchair( function()
		{
			this.save( { key: 'currentUni', uniName: CampusGuide.app.currentUni.get('name') }, function() { } );
			Ext.ComponentManager.get('currentUniLabel').setHtml("Current University: " + CampusGuide.app.currentUni.get('name'));
		});
	},
	
	onNoUniPick: function( picker, eOpts )
	{
		CampusGuide.app.currentUni = '';
	}
} );
