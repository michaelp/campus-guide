Ext.define( 'CampusGuide.model.Building',
{
	extend: 'Ext.data.Model',
	
	config:
	{
		fields:
		[
		{ name: 'name', type: 'string' }
		],
		belongsTo: 'University'
	}
} );
