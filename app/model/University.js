Ext.define( 'CampusGuide.model.University',
{
	extend: 'Ext.data.Model',
	config:
	{
		fields:
		[
			{ name: 'name', type: 'string' }
		],
		proxy:
		{
			type: 'ajax',
			url: 'resources/json/Universities.json',
			reader:
			{
				type: 'json',
				rootProperty: 'universities'
			}
		},
		hasMany: { model: 'CampusGuide.model.Building', name: 'buildings' }
	}
} );
