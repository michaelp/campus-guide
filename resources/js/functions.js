/*
 * Functions that can be used throughout Campus Guide
 */
 
/*
 * getCurrentLocation: get the location of the user
 * return: google.maps.LatLng, null on failure
 */
function getCurrentLocation(callback)
{
	navigator.geolocation.getCurrentPosition(
	function(position)
	{
		var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		callback(latLng);
	},
	function(error)
	{
		console.log("err" + error);
		callback(null);
	},
	{
	});
}
