/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides

//<debug>
Ext.Loader.setPath({
    'Ext': 'touch/src',
    'CampusGuide': 'app'
});
//</debug>

Ext.application(
{
	//Globals
	currentUni: 'Campus Guide',

	name: 'CampusGuide',

	requires: [
		'Ext.picker.Picker',
		'Ext.field.Select',
		'Ext.Label',
		'Ext.device.Geolocation',
		'Ext.util.Geolocation',
		'Ext.field.Search'
	],

	views:
	[
		'MainView',
		'MenuView',
		'CampusMapView',
		'OptionsView',
		'MainTabView',
		'FoodView',
		'BuildingsView',
		'BuildingButton',
		'BuildingRow',
		'SearchView',
		'BuildingListView'
	],
	controllers:
	[
		'CampusMapController',
		'MainController',
		'OptionsController',
		'BuildingController'
	],
	models:
	[
		'University',
		'Building'
	],
	stores:
	[
		'Universities'
	],

	icon: {
		'57': 'resources/icons/Icon.png',
		'72': 'resources/icons/Icon~ipad.png',
		'114': 'resources/icons/Icon@2x.png',
		'144': 'resources/icons/Icon~ipad@2x.png'
	},

	isIconPrecomposed: true,

	startupImage: {
		'320x460': 'resources/startup/320x460.jpg',
		'640x920': 'resources/startup/640x920.png',
		'768x1004': 'resources/startup/768x1004.png',
		'748x1024': 'resources/startup/748x1024.png',
		'1536x2008': 'resources/startup/1536x2008.png',
		'1496x2048': 'resources/startup/1496x2048.png'
	},

	launch: function() {
		//Load the store
		var uniStore = Ext.create( 'CampusGuide.store.Universities' );
		
		//Create a new lawnchair instance to see if they have visited the app before
		//Checking for a previous universite
		Lawnchair( function()
		{
			//Look for the key currentUni in Lawnchair
			this.exists( 'currentUni', function(exists)
			{
				if( exists )
				{
					//Find the university in uniStore
					this.get('currentUni', function(config)
					{
						var uni = uniStore.findRecord( 'uniName', config['name'] );
						CampusGuide.app.currentUni = uni;
						Ext.ComponentManager.get('currentUniLabel').setHtml("Current University: " + uni.get('name'));
					});
				}
				else
				{
					//Let them pick their university; give a suggestion based on their location
					//TODO: add a location based suggestion
					//pick your university
					var picker = Ext.create( 'Ext.picker.Picker',
					{
						listeners:
						{
							initialize: function( picker, eOpts )
							{
					
								uniStore.load( function()
								{
									var pickerSlots = new Array();
									for( var i = 0; i < uniStore.getCount(); i++ )
									{
										pickerSlots.push( { text: uniStore.getAt(i).get( 'name' ), value: uniStore.getAt(i) } );
									}
									picker.setSlots({
										name: 'unis',
										title: 'Universities',
										data: pickerSlots
									}); 
								} );
							}
						}
					} );
					Ext.Viewport.add( picker );
					picker.show();
				}
			});
		});

		// Initialize the main tab view
		Ext.Viewport.add(Ext.create('CampusGuide.view.MainView'));
	}
});
